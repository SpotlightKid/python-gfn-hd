#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Beispiel für Klasse mit Klassenvariablen und Methoden zur Kapselung des 
Attributzugriffs.
"""

class Car(object):
    color = 'red'
    velocity = 0
    engine_state = 0
    tire_profile = 2.2

    def start(self):
        if self.engine_state:
            print "Motor already running!"
        else:
            self.engine_state = 1
    
    def stop(self):
        if self.engine_state:
            if self.velocity != 0:
                print "You must bring the car to halt first."
            else:
                self.engine_state= 0
        else:
            print "Motor already off!"
    
    def accelerate(self, velocity=20):
        if not self.engine_state:
            print "You must start the engine first!"
            return
        if self.velocity + velocity > 120:
            print "Sorry, already cruising at full speed!"
        else:
            self.velocity += velocity
    
    def decelerate(self, velocity=20):
        if self.velocity - velocity < 0:
            velocity = self.velocity
        if velocity > 50:
            print "Iiiieeeeeee!"
            self.tire_profile -= 0.2        # ;-)
 
        self.velocity -= velocity

