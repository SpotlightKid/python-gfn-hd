#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Beispiel für einen Funktions-Dekorator.


# Äquivalenter Code ohne Decorator-Syntax

def add(a, b):
    return a + b

def wrapper(func):
    def printandadd(a, b):
        print "Rufe add() auf..."
        return func(a, b)
    return printandadd

add = wrapper(add)
print add(5, 7)

"""

import time

def wrapper(func):
    def timeit(a, b):
        start = time.time()
        res = func(a, b)
        print "Execution time: %f" % (time.time() - start)
        return res
    
    return timeit

@wrapper
def add(a, b):
    return a + b

@wrapper
def mul(a, b):
    return a * b


print add(5, 7)
print mul(5, 7)
