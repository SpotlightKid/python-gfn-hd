#!/usr/bin/env python
# -*- coding: latin1 -*-
"""Einfaches Beispiel f�r eine Objekt-Definition und eine Subklasse
(Buch- und Sachbuch-Klasse).

"""

class Buch(object):

    def __init__(self, titel, autor, typ):
        self.titel = titel
        self.autor = autor
        self.typ = typ

    def lesen(self, seite):
        self.lesezeichen = seite


class Sachbuch(Buch):

    def __init__(self, titel, autor):
        Buch.__init__(self, titel, autor, "Sachbuch")
        #self.name = "%s: %s" % (self.autor, self.titel)

    def lesen(self, seite):
        print("Ich bin schon viel schlauer!")
        Buch.lesen(self, seite)

    def __str__(self):
        return "Titel: %s\nAutor: %s\nTyp: %s" % (
            self.titel, self.autor, self.typ)

buch = Sachbuch("Zen und die Kunst des Bogenschie�ens", "Anonymous")
print(buch)
buch.lesen(30)
print(buch.lesezeichen)
