#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Einfaches Beispiel f�r eine Objekt-Definition (Address-Klasse)."""

class Address(object):

    def __init__(self, street, zipcode, city):
        self.street = street
        self.zipcode = zipcode
        self.city = city

    def drucken(self):
        print(self.street)
        print()
        print("%s %s" % (self.zipcode, self.city))

address = Address("M�hlbachstr. 20", 78351, 'Ludwigshafen/Bodensee')
address.drucken()
"""
Ausgabe:

M�hlbachstr. 20

78351 Ludwigshafen/Bodensee

"""
