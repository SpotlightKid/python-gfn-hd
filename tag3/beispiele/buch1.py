#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Einfaches Beispiel für eine Objekt-Definition (Buch-Klasse)."""

class Buch(object):

    def __init__(self, titel):
        self.titel = titel

    def lesen(self, page):
        self.merke_lesezeichen(page)

    def merke_lesezeichen(self, page):
        """Setzte Lesezeichen."""
        self.lesezeichen = page

buch = Buch("Der Steppenwolf")

print(buch.titel)

#print buch.lesezeichen  # Fehler
buch.lesen(50)
print buch.lesezeichen
buch2 = Buch("Der Herr der Ringe")
print buch2.titel
print Buch.titel  # Fehler
