#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Eine einfache Klasse für einen Vector-Datentyp.

Vector = (x1, y1)

(x1, y1) + (x2, y2) = (x1 + x2, y1 + y2)

(x1, y1) * (x2, y2) = x1 * x2 + y1 * y2

i * (x1, y1) = (i * x1, i * y1)

"""

class Vector(object):
    def __init__(self, x, y=None):
        if isinstance(x, Vector):
            self.x = x.x
            self.y = x.y
        else:
            self.x = x
            self.y = y

    def __str__(self):
        return "Vector(%i, %i)" % (self.x, self.y)

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)

    def __mul__(self, other):
        if isinstance(other, Vector):
            return self.x * other.x + self.y * other.y
        elif isinstance(other, int):
            return Vector(self.x * other, self.y * other)
        else:
            raise TypeError("Other side of expression must be in or Vector")

    __rmul__ = __mul__

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    @classmethod
    def fromvector(cls, v):
        return cls(v.x, v.y)

    #fromvector = classmethod(fromvector)


v1 = Vector(2, 8)
print(v1)

v2 = Vector(6, 4)
print(v1 + v2)
print(v1 + 10)
print(v1 * v2)
print(v1 * 5)
print(5 * v1)

v3 = Vector(v1)
print(v3 is v1)
print(v3 == v1)

v4 = Vector.fromvector(v1)
print(v4)
