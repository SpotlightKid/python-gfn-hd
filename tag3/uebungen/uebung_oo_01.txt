1) Definieren Sie eine Klasse "Person" mit folgenden Attributen:

- Vorname
- Nachname
- Adresse

Und folgender Methode:

- get_name()

die den vollen Vor- und Nachname als String zur�ckliefert.

2) Definieren Sie au�erdem eine '__str__'-Methode, die die Objektdaten als
formatierten String zur�ckgibt:

Name: Hans Meier
Adresse: ....

3) Erstellen Sie eine weitere Variante des Scripts aus der �bung vor der 
Mittagspause, die die Daten aus der Eingabedatei in Instanzen der Klasse 
'Person' speichert und sie folgenderma�en ausgibt:

    for person in personen:
        print person
