#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Beispiel für eine Klasse mit Property-Attribut."""

import sys
import codecs

class Person(object):

    def __init__(self, vorname, nachname, adresse):       #konstruktor
        self.vorname = vorname
        self.nachname = nachname
        self.adresse = adresse
        #self.name = self.Vorname + " " + self.Nachname

    #def get_name(self):
    #    return self.Vorname + " " + self.Nachname

    @property
    def name(self):
        return self.vorname + " " + self.nachname
    # oder:
    # name = property(name)

    def __str__(self):
        return "Name : %s\nAdresse: %s " % (self.name, self.adresse)

"""
p = Person("Hans", "Meier", '...')
p.Nachname = "Schulze"
p.name
"""

filename = 'adress.txt'
enc_name = 'latin1'

if len(sys.argv) > 1:
    filename = sys.argv[1]

if len(sys.argv) > 2:
    enc_name = sys.argv[2]

file = codecs.open(filename, 'r', enc_name)

adressbuch = []

for line in file:
    words = line.strip('\n').split(',', 2)

    words = [word.strip() for word in words]
    #name = words[0] + " " +  words[1]

    p1 = Person(*words)
    adressbuch.append(p1)

for person in adressbuch:
    print person
