#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import codecs


class Person(object):

    def __init__(self,vorname, nachname, adresse):
        self.vorname = vorname
        self.nachname = nachname
        self.adresse = adresse

    def get_name(self):
        return self.vorname + " " + self.nachname

    def __str__(self):
        return "Name : %s\nAdresse: %s " % (self.get_name(), self.adresse)


filename = 'data.txt'
enc_name = 'latin1'

if len(sys.argv) > 1:
    filename = sys.argv[1]

if len(sys.argv) > 2:
    enc_name = sys.argv[2]

file = codecs.open(filename, 'r', enc_name)

adressbuch = []

for line in file:
    words = line.strip('\n').split(',')

    words = [word.strip() for word in words]
    name = words[0].strip() + " " +  words[1].strip()

    person = Person(words[0], words[1], words[2])
    adressbuch.append(person)

for person in adressbuch:
    print(person)
