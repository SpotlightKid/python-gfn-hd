import random
import string

fo = open('test.txt', 'w')

line = ''.join(random.sample(string.ascii_lowercase, 26))

for i in range(500_000_000):
    fo.write(line + '\n')

fo.close()
