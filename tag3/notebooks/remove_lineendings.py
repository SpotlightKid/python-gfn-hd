def liesdiedatei(filename):
    infile = open(filename, 'r', encoding='utf-8')
    outfile = open('new_' + filename, 'w', encoding='utf-8')
    for line in infile:
        outfile.write(line.rstrip('\n'))
    infile.close()
    outfile.close()

liesdiedatei('test.txt')
