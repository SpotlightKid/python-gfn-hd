Objekt-orientierte Programmierung
---------------------------------

adress.txt
    Adressenliste als Kommaseparierte Textdatei

address.py
    Einfaches Beispiel für eine Objekt-Definition (Address-Klasse).

buch1.py
    Einfaches Beispiel für eine Objekt-Definition (Buch-Klasse).

buch2.py
    Einfaches Beispiel für eine Objekt-Definition und eine Subklasse
    (Buch- und Sachbuch-Klasse).

adressen_class_01.py
    Beispiel für eine Klasse für Adresseinträge.

adressen_class_02.py
    Beispiel für eine Klasse für Adresseinträge mit Property-Attribut.

example_decorator.py
    Beispiel für einen Funktions-Dekorator.

example_oo.txt
    Interaktive Python-Session mit Beispielen zur Verwendung von Objekten.

example_oo.py
    Beispiel für Klasse mit Klassenvariablen und Methoden zur Kapselung des
    Attributzugriffs.

uebung_oo_01.txt
    Übungsaufgabe zur Objektorientierung.

uebung_oo_02.txt
    Zusätzliche Übungsaufgabe zur Objektorientierung.

vector.py
    Eine einfache Klasse für einen Vector-Datentyp.
