Übung Adressen 2
================

Lesen Sie die folgende Liste elementweise in einer for-Schleife ein und
zerlegen Sie jedes Stringelement an den Kommata in drei (3) Stringwerte.

    data = [
        "Mueller,Thomas,Buchhaltung",
        "Meier,Hans,Entwicklung",
        "Wagner,Petra,Einkauf",
        "Schneider,Harald,Entwicklung",
        "Fuchs,Stefanie,Buchhaltung"
    ]

Speichern Sie diese Werte in einem Dictionary mit den Keys 'nachname', vorname'
und 'abteilung'.

Sammeln sie alle erzeugten Dictionaries in einer Liste und geben sie diese am
Ende elementweise aus.

Die Ausgabe pro Eingabezeile soll folgendermaßen aussehen:

    Eintrag Nummer 0x:

    Name: Thomas Mueller
    Abteilung: Buchhaltung

    Eintrag Nummmer ...

Die Nummerierung der Einträge soll mit eins (1) starten und zweistellig, ggf.
führender Null, ausgegeben werden!
