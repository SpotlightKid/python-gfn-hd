# Generelle Struktur von Dateifiltern

import sys

with open(sys.argv[1]) as datei:
    for line in datei:
        # match_filter sei eine Funktion, die einen String als Argument nimmt
        # und wieder einen String (möglicherweise leer) zurück gibt
        sys.stdout.write(match_filter(line))
