#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import codecs

filename = 'data.txt'
enc_name = 'latin1'

if len(sys.argv) > 1:
    filename = sys.argv[1]

if len(sys.argv) > 2:
    enc_name = sys.argv[2]

file = codecs.open(filename, 'r', enc_name)

adressbuch = []

for line in file:
    words = line.split(',')
    name = words[0].strip() + " " +  words[1].strip()

    while len(name) < 30:
        name = name + " "

    adressbuch.append(
        {
            'Nachname': words[0].strip(),
            'Vorname': words[1].strip(),
            'Adresse': words[2],
            'Name': name
        }
    )

for item in adressbuch:
    print("Name: %(Name)s Adresse: %(Adresse)s" % item)

print("")

for i, item in enumerate(adressbuch):
    item['no'] = i + 1
    print("Eintrag No. %(no)02i\n\nName: %(Nachname)s\nVorname: %(Vorname)s\nAdresse: %(Adresse)s" % item)
