import sys
import codecs

filename = 'data.txt'
enc_name = 'latin1'

if len(sys.argv) > 1:
    filename = sys.argv[1]

if len(sys.argv) > 2:
    enc_name = sys.argv[2]

file = codecs.open(filename, 'r', enc_name)

adressbuch = []

for line in file:
    words = line.strip('\n').split(',')

    words = [word.strip() for word in words]

    name = words[0] + " " + words[1]
    adressbuch.append(
        {
            'Nachname': words[0],
            'Vorname': words[1],
            'Adresse': words[2],
            'Name': name
        }
    )

for item in adressbuch:
    print("Name: %(Name)-23s Adresse: %(Adresse)s" % item)

print("")

for i, item in enumerate(adressbuch):
    item['no'] = i + 1
    print("Eintrag No. %(no)02i\n\nName: %(Nachname)s\nVorname: %(Vorname)s\nAdresse: %(Adresse)s\n" % item)
