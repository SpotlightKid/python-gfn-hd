# -*- coding: utf-8 -*-

# Lesen Sie die folgende Liste elementweise in einer for-Schleife
# ein und zerlegen Sie jedes Stringelement an den Kommata in 3 Stringwerte.
#
# Speichern Sie diese Werte in einem Dictionary mit den Keys
# 'nachname', vorname' und 'abteilung'.
#
# Sammeln sie alle erzeugten Dictionaries in einer Liste und geben
# Sie diese am Ende mit ``print`` aus.

data = [
    "Mueller,Thomas,Buchhaltung",
    "Meier,Hans,Entwicklung",
    "Wagner,Petra,Einkauf",
    "Schneider,Harald,Entwicklung",
    "Fuchs,Stefanie,Buchhaltung"
]

result = []

for line in data:
    values = line.split(',')
    record = dict()
    record['nachname'] = values[0]
    record['vorname'] = values[1]
    record['abteilung'] = values[2]

    result.append(record)

print(result)
