# -*- coding: utf-8 -*-

# Lesen Sie die Datei ``data.txt`` zeilenweise in einer for-Schleife ein und
# zerlegen Sie jede Zeile an den Kommata in drei(3) Stringwerte. Nicht korrekt
# formatierte Zeilen in der Eingabedatei sollen zum Abbruch des Programs mit
# einer Fehlermeldung nach folgendem Beispiel führen:
#
#    Invalid line (0001): Falsch,Fabian"
#
# Speichern Sie diese Werte in einem Dictionary mit den Keys
# 'nachname', vorname' und 'abteilung'.
#
# Sammeln sie alle erzeugten Dictionaries in einer Liste und geben sie diese
# am Ende elementweise aus (Format siehe unten).

datei = open('data.txt')

result = []
for i, line in enumerate(datei):
    try:
        values = line.rstrip().split(',', 2)
        record = dict(vorname=values[1], nachname=values[0], abteilung=values[2])

        result.append(record)
    except:
        print("Invalid line (%04i): %s" % (i, line))
        raise

# Die Ausgabe pro Eingabezeile soll folgendermaßen aussehen:
#
#     Eintrag Nummer 0x:
#
#     Name: Thomas Mueller
#     Abteilung: Buchhaltung
#
#     Eintrag Nummmer ...

for i, d in enumerate(result):
    print("Eintrag No. %02i" % i)
    print()
    print("Name: %(vorname)s %(nachname)s" % d)
    print("Abteilung: %(abteilung)s" % d)
    print()
