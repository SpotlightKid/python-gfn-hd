# -*- coding: utf-8 -*-

# Lesen Sie die folgende Liste elementweise in einer for-Schleife
# ein und zerlegen Sie jedes Stringelement an den Kommata in 3 Stringwerte.
#
# Speichern Sie diese Werte in einem Dictionary, dessen Keys sich aus
# den Werten aus der ersten Zeile der Liste ergeben.
#
# Sammeln sie alle erzeugten Dictionaries in einer Liste und geben
# Sie diese am Ende mit ``print`` aus.

data = [
    "lastname,firstname,department",
    "Mueller,Thomas,Buchhaltung",
    "Meier,Hans,Entwicklung",
    "Wagner,Petra,Einkauf",
    "Schneider,Harald,Entwicklung",
    "Fuchs,Stefanie,Buchhaltung"
]

result = []
keys = data[0].split(',')
# Zur Kontrolle
print(keys)

for line in data[1:]:
    values = line.split(',')
    record = dict()
    record[keys[0]] = values[0]
    record[keys[1]] = values[1]
    record[keys[2]] = values[2]

    result.append(record)

print(result)
