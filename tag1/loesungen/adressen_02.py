# -*- coding: utf-8 -*-

# Lesen Sie die folgende Liste elementweise in einer for-Schleife
# ein und zerlegen Sie jedes Stringelement an den Kommata in 3 Stringwerte.
#
# Speichern Sie diese Werte in einem Dictionary mit den Keys
# 'nachname', vorname' und 'abteilung'.
#
# Sammeln sie alle erzeugten Dictionaries in einer Liste und geben sie diese
# am Ende elementweise aus (Format siehe unten).


data = [
    "Mueller,Thomas,Buchhaltung",
    "Meier,Hans,Entwicklung",
    "Wagner,Petra,Einkauf",
    "Schneider,Harald,Entwicklung",
    "Fuchs,Stefanie,Buchhaltung"
]

result = []
for line in data:
    values = line.split(',')
    record = dict(vorname=values[1], nachname=values[0], abteilung=values[2])
    result.append(record)

# Die Ausgabe pro Eingabezeile soll folgendermaßen aussehen:
#
#     Eintrag Nummer 0x:
#
#     Name: Thomas Mueller
#     Abteilung: Buchhaltung
#
#     Eintrag Nummmer ...

for i, record in enumerate(result):
    print("Eintrag No. %02i" % (i + 1,))
    print()
    print("Name: %(vorname)s %(nachname)s" % record)
    print("Abteilung: %(abteilung)s" % record)
    print()
