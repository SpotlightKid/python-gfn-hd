#!/usr/bin/env python

# for Python 2
from __future__ import print_function, unicode_literals

import sys


def main():
    print("Hallo, %s" % sys.argv[1])  # command line args start at index 1!


if __name__ == '__main__':
    main()
