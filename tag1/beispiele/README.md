Einfache Python-Beispieldateien
-------------------------------

* `helloworld.py` - Ein minmales "Hello, World!" Skript

* `pythagoras.py` - Ein Beispiel mit etwas mehr Syntax-Features

* `basicscript.py` - Ein minimales Kommandozeilenprogramm

* `modtemplate.py` - Eine Vorlage für Pythonmodule

* `script-template.py` - Eine Vorlage für Kommandozeilenskripte
