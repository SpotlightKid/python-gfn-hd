#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# pythagoras.py
#
"""Get the length of the two sides of a right-angled triangle and print the
length of the hypotenuse."""

import math


def hypotenuse(a, b):
    """Compute the hypotenuse given length of a and b."""
    c = math.sqrt(a**2 + b**2)
    return c


if __name__ == '__main__':
    while True:
        a = input("Enter length of 'a': ")
        b = input("Enter length of 'b': ")
        try:
            print("Length of 'c': %f" % hypotenuse(float(a), float(b)))
            print('')
        except ValueError:
            print("Invalid input!")
            break
