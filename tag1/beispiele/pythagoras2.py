#!/usr/bin/env python3
# coding: utf-8
#
# pythagoras.py
#
"""Get the length of the two sides of a right-angled triangle and print the
length of the hypotenuse."""

import math


#~def hypotenuse(a, b):
#~    """Compute the hypotenuse given length of a and b."""
#~    c = math.sqrt(a**2 + b**2)
#~    return c


while True:
    input_a = input("Enter length of 'a': ")
    input_b = input("Enter length of 'b': ")
    try:
        print("Length of 'c': %f" % math.sqrt(float(input_a)**2 + float(input_b)**2))
        print('')
    except ValueError:
        print("Invalid input!")
        break
