#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script/Module level documentation string."""

# for Python 2
from __future__ import print_function, unicode_literals

# standard library import
import sys

__all__ = (
    'main'
)

def main(args=None):
    """Script main function documentation string."""
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]) or 0)
