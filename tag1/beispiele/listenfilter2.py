#!/usr/bin/env python
# -*- coding: utf-8 -*-

infile = 'data.txt'

fp = open(infile)

result = []
for adresse in fp:
    adresse = adresse.rstrip('\n')
    values = adresse.split(',')
    if 'entwicklung' == values[-1].lower():
        result.append(adresse)

print(result)
fp.close()
