#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script/Module level documentation string."""

# standard library imports
import sys

__all__ = (
    'foo'
)

def foo():
    """Docstring for function foo."""
    pass

# "private" functions
def _test():
    """Test function."""
    return 0


if __name__ == '__main__':
    # This is only executed when the module is run as a script
    _test()
