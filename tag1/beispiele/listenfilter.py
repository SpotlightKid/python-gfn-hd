#!/usr/bin/env python
# -*- coding: utf-8 -*-

data = [
   "Chris,Arndt,Koeln",
   "Michael,Feigl,München",
   "Jochen,Goßmann,Heidelberg",
   "Hans,Heidelberg,Frankfurt",
]

result = []
for adresse in data:
    values = adresse.split(',')
    if 'heidelberg' == values[-1].lower():
        result.append(adresse)

print(result)
