d = {
   'name': 'Chris',
   'email': 'chris@chrisarndt.de',
   'city': 'Koeln'
}

for key in d:
    print("Key: %s" % key)
    print("Value: %s" % d[key])
