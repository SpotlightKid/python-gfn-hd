def power(a, b):
    return a ** b

def div(a, b):
    return a / b

def mul(a, b):
    return a * b

def cap(a, min=0, max=100):
    if a < min:
        return min
    elif a > max:
        return max
    else:
        return a

def foo():
    pass
