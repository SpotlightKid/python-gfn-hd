#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

import requests

# für GFN Proxy
os.environ['HTTP_PROXY'] = '192.168.254.5:8080'
os.environ['HTTPS_PROXY'] = '192.168.254.5:8080'

BASE_URL = 'http://maps.googleapis.com/maps/api/geocode/json'


def main(args):
    if not args:
        return "usage: geocode SEARCHTERM..."
    else:
        searchterm = " ".join(args)

    url_params = {'address': searchterm, 'language': 'en'}
    response = requests.get(BASE_URL, params=url_params)
    data = response.json()

    for result in data['results']:
        print("Adresse:", result["formatted_address"])
        print("Typ:", ", ".join(result['types']))
        print("Länge:", result['geometry']['location']['lat'])
        print("Breite:", result['geometry']['location']['lng'])
        print()


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]) or 0)
