#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
import re

args = sys.argv[1:]
regex = args.pop(0)
files = args

for filename in files:
    fobj = open(filename)
    for line in fobj:
        match = re.search(regex, line)
        if match:
            print(filename, ':', line, end='')
