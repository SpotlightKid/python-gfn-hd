#!/usr/bin/env python
"""
Setze für alle Dateien in einem Verzeichnis, welche die Dateieindung '.jpg'
oder '.jpeg' haben (case-insensitiv), den JPG-Kommentar.

Das Zielverzeichnis und der Kommentar müssen als Kommandozeilenparameter an das
Skript übergeben werden.
"""

import glob
import os
import subprocess
import sys

from os.path import isdir, join, splitext


EXIV2 = 'exiv2'

if sys.platform.startswith('win'):
    EXIV2 += '.exe'

if getattr(sys, 'frozen', None):
    EXIV2 = join(sys._MEIPASS, 'exiv2', EXIV2)


def set_jpg_comment(filename, comment):
    """Setzt den JPG-Kommentar in Datei 'filename' auf den Wert von 'comment'."""
    return subprocess.call([EXIV2, 'mo', '-c', comment, filename])


def find_jpgs(destdir):
    """Gibt eine Liste von Pfaden im Verzeichnis 'destdir' der Dateien,
    welche die Extension '.jpg' oder 'jpeg' haben (case-insensitiv), zurück.
    """
    result = []
    for path in sorted(glob.glob(destdir + '/*')):
        if isdir(path):
            continue
        elif splitext(path)[1].lower() in ('.jpg', '.jpeg'):
            result.append(path)
    return result


def main(args):
    if len(args) < 2:
        sys.exit("Setze Kommandozeilenparameter: <dir> <comment>")
    else:
        destdir = args[0]
        comment = args[1]

    if isdir(destdir):
        for jpgpath in find_jpgs(destdir):
            print("Setze JPG Kommentar in Datei", jpgpath)
            if set_jpg_comment(jpgpath, comment) != 0:
                print("Return code nicht null!")


if __name__ == '__main__':
    main(sys.argv[1:])
